﻿using UnityEngine;


public class Singleton<T> : MonoBehaviour where T : Singleton<T> {

    public bool dontDestroyOnLoad_ = false;

    private static T Instance_;
    public static T instance_ {
        get {
            if(Instance_ == null) {
                Instance_ =(T)FindObjectOfType(typeof(T));
            }
            
            return Instance_;
        }
        protected set {
            Instance_ = value;
        }
    }


    public static bool initialized_;


    protected virtual void Awake() {
        if(Instance_ != null && Instance_ != this) {
            Destroy(gameObject);
            return;
        }

        Instance_ =(T)FindObjectOfType(typeof(T));

        if (dontDestroyOnLoad_) {
            DontDestroyOnLoad(gameObject);
        }
    }


    public virtual void Initialize() {
        initialized_ = true;
    }
}