﻿using System;

using UnityEngine;


public class TimeStampManager : Singleton<TimeStampManager> {
        
    public static DateTime startupTime_ {
        get {
            return startupTime;
        }
    }
    private static DateTime startupTime;

    private static DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

        
    public static DateTime CurrentTime {
        get {
            return startupTime.AddSeconds((DateTime.UtcNow - startupTime).TotalSeconds);
        }
    }


    public static double CurrentTimestamp {
        get {
            return ((startupTime.AddSeconds((DateTime.UtcNow - startupTime).TotalSeconds)) - epoch).TotalSeconds;
        }
    }


    public static double TargetTimeStamp(DateTime time) {
        TimeSpan elapsed = time - epoch;
        return elapsed.TotalSeconds;
    }
        

    protected override void Awake() {
        base.Awake();

        startupTime = DateTime.UtcNow;
    }
}