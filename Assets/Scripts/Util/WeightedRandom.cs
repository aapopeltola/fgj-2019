﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

namespace Util {
    public static class WeightedRandom {

        public static int GetWeightedRandomIndex(float[] weights) {
            float totalWeights = 0;
            float minValue = float.MaxValue;
            float maxValue = float.MinValue;

            for(int i = 0; i < weights.Length; i++) {
                totalWeights += Mathf.Abs(weights [i]);
                if(minValue > weights [i]) {
                    minValue = weights [i];
                } else if(maxValue < weights [i]) {
                    maxValue = weights [i];
                }
            }

            float random = GetRandom(0, totalWeights);

            for(int i = 0; i < weights.Length; i++) {
                float currentValue = weights [i];

                random -= currentValue;

                if(random <= 0) {
                    return i;
                }
            }

            return weights.Length - 1;
        }


        public static T GetRandom<T>(float[] weights, List<T> list) {
            return list [GetWeightedRandomIndex(weights)];
        }


        private static float GetRandom(float min, float max) {
            Guid guid = Guid.NewGuid();

            System.Random random = new System.Random(guid.GetHashCode());

            return(random.Next((int)(min * 10000),(int)(max * 10000))) / 10000f;
        }


        private static int GetRandom(int min, int max) {
            int seed = DateTime.Now.Millisecond;

            System.Random random = new System.Random(seed);

            return(random.Next(min, max));
        }
    }
}