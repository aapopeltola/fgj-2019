﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Events;

public delegate float TransitionFunction(float time, float duration);
public delegate void UpdateFunction(float currentValue, float currentTime);

public enum MovementSpace {
    WORLD = 0,
    LOCAL = 1,
    ANCHORED = 2
}

namespace Util{
    public static class Transitions {

        public static IEnumerator Transition(float duration, UpdateFunction updateFunction, TransitionFunction transitionFunction = null) {
            float time = 0.0f;
            float currentValue = 0.0f;

            if (transitionFunction == null) {
                transitionFunction = new TransitionFunction(Linear);
            }

            WaitForEndOfFrame waitEndOfFrame = new WaitForEndOfFrame ();

            while (time < duration) {
                currentValue = transitionFunction (time, duration);
                updateFunction (currentValue, time);
                time += Time.deltaTime;

                yield return waitEndOfFrame;
            }

            updateFunction (1f, 1f);
        }


        public static IEnumerator Move(float duration, 
                                       Transform target,
                                       Vector3 start, 
                                       Vector3 end,
                                       MovementSpace space = MovementSpace.WORLD,
                                       UnityAction endAction = null,
                                       TransitionFunction transitionFunction = null) {
            Vector3 difference = end - start;
            yield return Transition (
                duration,
                (float currentValue, float currentTime) => {
                    if (space == MovementSpace.WORLD) {
                        target.position = start + difference * currentValue;
                    }
                    else if (space == MovementSpace.LOCAL) {
                        target.localPosition = start + difference * currentValue;
                    }
                    else if (space == MovementSpace.ANCHORED) {
                        ((RectTransform)target).anchoredPosition = start + difference * currentValue;
                    }

                    if (currentTime == 1f) {
                        if (endAction != null) {
                            endAction();
                        }
                    }
                },
                transitionFunction
            );
        }


        public static IEnumerator Scale(float duration, 
                                        Transform target,
                                        Vector3 start, 
                                        Vector3 end,
                                        UnityAction endAction = null,
                                        TransitionFunction transitionFunction = null) {
            Vector3 difference = end - start;
            yield return Transition (
                duration,
                (float currentValue, float currentTime) => {
                    target.localScale = start + difference * currentValue;

                    if (currentTime == 1f) {
                        if (endAction != null) {
                            endAction();
                        }
                    }
                },
                transitionFunction
            );
        }


        public static IEnumerator EulerRotate(float duration, 
                                              Transform target,
                                              Vector3 start, 
                                              Vector3 end,
                                              UnityAction endAction = null,
                                              MovementSpace space = MovementSpace.WORLD,
                                              TransitionFunction transitionFunction = null) {
            Vector3 difference = end - start;
            yield return Transition (
                duration,
                (float currentValue, float currentTime) => {
                    if (space == MovementSpace.WORLD) {
                        target.eulerAngles = start + difference * currentValue;
                    }
                    else if (space == MovementSpace.LOCAL) {
                        target.localEulerAngles = start + difference * currentValue;
                    }
                    else if (space == MovementSpace.ANCHORED) {
                        Debug.LogWarning("No such thing as anchored euler rotation");
                        currentTime = 1f;
                    }

                    if (currentTime == 1f) {
                        if (endAction != null) {
                            endAction();
                        }
                    }
                },
                transitionFunction
            );
        }


        public static IEnumerator CurvedMove(float duration, 
                                             Transform target,
                                             Vector3 start, 
                                             Vector3 end,
                                             Vector3 offset,
                                             MovementSpace space = MovementSpace.WORLD,
                                             UnityAction endAction = null,
                                             TransitionFunction transitionFunction = null) {
            int pathMarkers = Mathf.CeilToInt (duration * (1f / Time.fixedDeltaTime));

            Vector3[] curvedPath = Util.General.GetCurvedPosition (
                start,
                end,
                offset,
                pathMarkers
            );

            Debug.Log ("curved paths " + curvedPath.Length);

            int currentIndex = 0;

            yield return Transition (
                duration,
                (float currentValue, float currentTime) => {
                    currentIndex = Mathf.Clamp(Mathf.FloorToInt(pathMarkers * currentValue), 0, curvedPath.Length - 1);

                    Debug.Log("current index " + currentIndex);

                    if (space == MovementSpace.WORLD) {
                        target.position = curvedPath[currentIndex];
                    }
                    else if (space == MovementSpace.LOCAL) {
                        target.localPosition = curvedPath[currentIndex];
                    }
                    else if (space == MovementSpace.ANCHORED) {
                        ((RectTransform)target).anchoredPosition = curvedPath[currentIndex];
                    }

                    if (currentTime == 1f) {
                        if (endAction != null) {
                            endAction();
                        }
                    }
                },
                transitionFunction
            );
        }


        public static IEnumerator CurvedCubicMove(float duration, 
                                                  Transform target,
                                                  Vector3 start, 
                                                  Vector3 end,
                                                  Vector3 offset,
                                                  MovementSpace space = MovementSpace.WORLD,
                                                  UnityAction endAction = null,
                                                  TransitionFunction transitionFunction = null) {
            int pathMarkers = Mathf.CeilToInt (duration * (1f / Time.fixedDeltaTime));

            Vector3 direction = (end - start).normalized;
            float distance = Vector3.Distance (start, end);

            Vector3 startOffset = start + offset + direction * distance * 0.4f;
            Vector3 endOffset = end + offset - direction * distance * 0.4f;

            Vector3[] curvedPath = Util.General.GetCubicCurvedPosition (
                start,
                end,
                startOffset,
                endOffset,
                pathMarkers
            );

            int currentIndex = 0;

            yield return Transition (
                duration,
                (float currentValue, float currentTime) => {
                    currentIndex = Mathf.Clamp(Mathf.FloorToInt(pathMarkers * currentValue), 0, curvedPath.Length - 1);

                    if (space == MovementSpace.WORLD) {
                        target.position = curvedPath[currentIndex];
                    }
                    else if (space == MovementSpace.LOCAL) {
                        target.localPosition = curvedPath[currentIndex];
                    }
                    else if (space == MovementSpace.ANCHORED) {
                        ((RectTransform)target).anchoredPosition = curvedPath[currentIndex];
                    }

                    if (currentTime == 1f) {
                        if (endAction != null) {
                            endAction();
                        }
                    }
                },
                transitionFunction
            );
        }
		

        public static float EaseIn(float time, float duration) {
            float localTime = time / duration;
            
            return (localTime * localTime * localTime);
        }


        public static float EaseInOut(float time, float duration) {
            float localTime = time / (duration / 2.0f);
            
            if (localTime < 1.0f) {
                return 0.5f * localTime * localTime * localTime;
            }
            else {
                localTime -= 2.0f;
                return 0.5f * (localTime * localTime * localTime + 2.0f);
            }
        }


        public static float EaseOut(float time, float duration) {
            float localTime = (time / duration) - 1.0f;
            
            return localTime * localTime * localTime + 1.0f;
        }


        public static float Linear(float time, float duration) {
            return time / duration;
        }
    }
}