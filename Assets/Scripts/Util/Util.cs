﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;

public enum Layers
{
    Default = 0,
    TransparentFX = 1,
    IgnoreRaycast = 2,
    Water = 4,
    UI = 5,
    Player = 8,
    Projectiles = 9,
    Enemy = 10,
    DeadEnemy = 11,
}

namespace Util {
    public static class General {
        private static System.Random random = new System.Random(new Guid().GetHashCode());

        public static string SecondsToShortTime(float seconds, bool showMilliseconds = true) {
            TimeSpan t = TimeSpan.FromSeconds (Mathf.Abs(seconds));
            string s = "";

            // If seconds is higher than 24 hours, show days and hours
            if (seconds > 86400) {
                s = string.Format ("{0:D2}D:{1:D2}H", t.Days, t.Hours);
            }
            // If seconds is higher than one hour, show hours and minutes
            else if (seconds > 3600) {
                s = string.Format ("{0:D2}H:{1:D2}M", t.Hours, t.Minutes);
            }
            // If seconds is higher than one minute, show minutes and seconds
            else if (seconds > 60) {
                s = string.Format ("{0:D2}M:{1:D2}S", t.Minutes, t.Seconds);
            }
            else {
                if (showMilliseconds) {
                    s = string.Format ("{0:D2}:{1:D3}", t.Seconds, t.Milliseconds);
                }
                else {
                    s = string.Format ("{0:D2}M:{1:D2}S", t.Minutes, t.Seconds);
                }
            }

            return s;
        }
		
		
        /// <summary>
        /// Gets the curved position vector array.
        /// </summary>
        /// <returns>Array of vector 3 positions.</returns>
        /// <param name="start">Start position</param>
        /// <param name="end">End position</param>
        /// <param name="offset">Offset amount</param>
        public static Vector3[] GetCurvedPosition(Vector3 start, Vector3 end, Vector3 offset, int points = 10) {
            Vector3[] positions = new Vector3[points];
            positions [0] = start;
            
            for (int i = 1; i < positions.Length - 1; i++) {
                float t = i * (1f / (float)points);
                
                positions [i] = (
                    Mathf.Pow (1f - t, 2) * start
                    + 2f * (1f - t) * t * offset
                    + Mathf.Pow (t, 2) * end
                );
            }
            
            positions [positions.Length - 1] = end;
            
            return positions;
        }


        /// <summary>
        /// Gets the cubic curved position vector array.
        /// </summary>
        /// <returns>Array of vector 3 positions.</returns>
        /// <param name="start">Start position</param>
        /// <param name="end">End position</param>
        /// <param name="offset">Offset amount</param>
        public static Vector3[] GetCubicCurvedPosition(Vector3 start, Vector3 end, Vector3 offsetA, Vector3 offsetB, int points = 10) {
            Vector3[] positions = new Vector3[points];
            positions [0] = start;

            for (int i = 1; i < positions.Length - 1; i++) {
                float t = i * (1f / (float)points);

                positions [i] = (
                    Mathf.Pow (1f - t, 3) * start
                    + 3f * Mathf.Pow(1f - t, 2) * t * offsetA
                    + 3f * (1f - t) * Mathf.Pow(t, 2) * offsetB
                    + Mathf.Pow (t, 3) * end
                );
            }

            positions [positions.Length - 1] = end;

            return positions;
        }


        /// <summary>
        /// Takes an array of vectors and calculates the middle point of these vectors.
        /// </summary>
        /// <returns>The center of given vectors.</returns>
        /// <param name="positions">Vector positions</param>
        public static Vector3 MiddleOfVectors (Vector3[] positions) {
            float x = 0, y = 0, z = 0;
            int i = 0;
            for (i = 0; i < positions.Length; i++) {
                x += positions [i].x;
                y += positions [i].y;
                z += positions [i].z;
            }
            return new Vector3 (x / i, y / i, z / i);
        }


        /// <summary>
        /// Takes array of vectors and calculates the average distance to the middle point of these vectors
        /// </summary>
        /// <returns>The average distance to the middle</returns>
        /// <param name="positions">Vector positions</param>
        public static float AverageDistanceOfVectorsToMiddle (Vector3[] positions) {
            float distance = 0;
            int i = 0;
            Vector3 middle = MiddleOfVectors (positions);
            for (i = 0; i < positions.Length; i++) {
                distance += Vector3.Distance (positions [i], middle);
            }
            return distance / i;
        }


        public static string LayerToString (Layers layer) {
            return layer.ToString ();
        }


        public static int LayerToInt (Layers layer) {
            return (int)layer;
        }


        public static bool IsOnLayer (string myLayer, Layers layer) {
            if (myLayer.Equals(layer.ToString()))
                return true;
            else
                return false;
        }


        public static Vector3 GetRandomPositionOnUnitSphere(Vector3 origin, float radius, bool isOnNavMesh = false) {
            Vector3 output = origin;
            UnityEngine.AI.NavMeshHit navMeshHit;
            if (isOnNavMesh) {
                int tries = 0;
                do {
                    Vector3 newVector = origin + (UnityEngine.Random.onUnitSphere * radius);

                    if (UnityEngine.AI.NavMesh.SamplePosition(newVector, out navMeshHit, 0.1f, UnityEngine.AI.NavMesh.AllAreas)) {
                        output = newVector;
                    }

                    tries++;
                }
                while (output == origin && tries < 1000);
            }
            else {
                output = origin + (UnityEngine.Random.onUnitSphere * radius);
            }

            return output;
        }
		

		/*!
         * Shuffle a list.
         */
        public static void Shuffle<T>(this IList<T> list) {
            int n = list.Count;
            while (n > 1) {
                --n;
                int k = random.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }



        public static int GetVersionInteger(string versionString) {
            // Index 0 is be major, 1 is minor and 2 patch.
            string[] versionSplit = versionString.Split(new char[] {'.', ':'});

            string versionInteger = (
                versionSplit[0].PadLeft(2, '0')
                + versionSplit[1].PadLeft(2, '0')
                + versionSplit[2].PadLeft(2, '0')
            );

            return Convert.ToInt32(versionInteger);
        }
    }
}