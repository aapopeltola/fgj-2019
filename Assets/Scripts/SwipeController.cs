﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SwipeDirection
{
    up,
    down,
    right,
    left,
    none
}

public class SwipeController : MonoBehaviour
{
    private Vector3 delta;
    private Vector3 origin;
    private float absX, absY;
    private SwipeDirection currentSwipe;

    private Action swipeFunction;
    private Func<Vector3, SwipeDirection> detectSwipeFunc;

    public float thresholdForSwipe = 1f;

    public static Action SwipeUp = ()=> { };
    public static Action SwipeDown = ()=> { };
    public static Action SwipeRight = ()=> { };
    public static Action SwipeLeft = () => { };

    private Touch[] fingers;
    private bool swipeInitiated = false;

    private void Awake()
    {
//#if UNITY_EDITOR
//        swipeFunction = () => Swipe();
//#else
        swipeFunction = () => TouchSwipe();
//#endif
        detectSwipeFunc = (x) => DetectSwipeSingle(x);
        //for mutliplayer where camera is rotated. Swipe detection has been changed also.
        //detectSwipeFunc = (x) => DetectSwipeMulti(x);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(swipeFunction != null)
        {
            swipeFunction();
        }
    }

    private void TouchSwipe() {
        if(Input.touchCount >= 2) {
            fingers = Input.touches;
            delta = fingers[1].deltaPosition;
            swipeInitiated = true;
        }

        if(Input.touchCount <= 1 && swipeInitiated) {
            swipeInitiated = false;
            currentSwipe = detectSwipeFunc(delta);
            CallForAction(currentSwipe, delta);
        }
    }

    private void Swipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            origin = Input.mousePosition;
        }

        if (Input.GetMouseButton(0))
        {
            delta = Input.mousePosition - origin; 
        }

        if (Input.GetMouseButtonUp(0))
        {
  
            currentSwipe = detectSwipeFunc(delta);
            CallForAction(currentSwipe, delta);
        }
    }

    private void CallForAction(SwipeDirection swipe, Vector3 delta)
    {
        switch (swipe)
        {
            case SwipeDirection.up:
                if (SwipeUp != null) SwipeUp();
                break;
            case SwipeDirection.down:
                if (SwipeDown != null) SwipeDown();
                break;
            case SwipeDirection.right:
                if (SwipeRight != null) SwipeRight();
                break;
            case SwipeDirection.left:
                Debug.Log("Left");
                if (SwipeLeft != null) SwipeLeft();
                break;
            case SwipeDirection.none:
            default:
                break;
        }
    }

    private SwipeDirection DetectSwipeSingle(Vector3 delta)
    {
        absX = Mathf.Abs(delta.x);
        absY = Mathf.Abs(delta.y);

        if (absX > absY && absX >= thresholdForSwipe)
        {
            if (delta.x > 0)
            {               
                //horizontal
                return SwipeDirection.right;
            }
            else if (delta.x < 0)
            {
                return SwipeDirection.left;
            }
        
        }
        else if(absY > absX && absY >= thresholdForSwipe)
        {
            //vertical
            if (delta.y > 0)
            {
                return SwipeDirection.up;
            }
            else if (delta.y < 0)
            {
                return SwipeDirection.down;
            }
        }

        return SwipeDirection.none;
    }

    private SwipeDirection DetectSwipeMulti(Vector3 delta)
    {
        absX = Mathf.Abs(delta.x);
        absY = Mathf.Abs(delta.y);

        if (absX > absY && absX >= thresholdForSwipe)
        {
            if (delta.x > 0)
            {
                //horizontal
                return SwipeDirection.up;
            }
            else if (delta.x < 0)
            {
                return SwipeDirection.down;
            }

        }
        else if (absY > absX && absY >= thresholdForSwipe)
        {
            //vertical
            if (delta.y > 0)
            {
                return SwipeDirection.right;
            }
            else if (delta.y < 0)
            {
                return SwipeDirection.left;
            }
        }

        return SwipeDirection.none;
    }
}
