﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;
using UnityEngine.UI;

public enum Lane {
    up,
    down
}

public class CharacterMechanism : MonoBehaviour
{
    private Lane currentPos_ = Lane.down;
    private bool canDash = true;
    private SkeletonAnimation skele;
    private MeshRenderer mr;
    private Vector3 lowerLanePos;
    private Vector3 lowerLaneScale;
    private MeshRenderer[] shade_renderers;
    private CapsuleCollider2D charCollider;

    public Vector3 upperLanePos;
    public Vector3 upperLaneScale;

    public float dashcooldown = 5f;
    public float dashDuration = 3f;
    public GameObject shades, cooldownInd;

    public bool slowdownEffectOn = false;
    // Start is called before the first frame update
    void Start()
    {
        lowerLanePos = this.transform.position;
        lowerLaneScale = this.transform.localScale;
        skele = GetComponent<SkeletonAnimation>();
        mr = GetComponent<MeshRenderer>();
        shade_renderers = GetShadeRenderers();

        charCollider = GetComponent<CapsuleCollider2D>();

        SwipeController.SwipeDown = () => GoDown();
        SwipeController.SwipeLeft = () => Dash();
        SwipeController.SwipeUp = () => GoUp();
    }


    // Update is called once per frame
    void Update() {

    }

    public Lane GetCurrentLane() {
        return currentPos_;
    }

    private MeshRenderer[] GetShadeRenderers() {
        List<MeshRenderer> mrs = new List<MeshRenderer>(GetComponentsInChildren<MeshRenderer>(true));
        mrs.RemoveAll(x => x.transform == transform);
        return mrs.ToArray();
    }

    private void GoUp() {
        if(currentPos_ == Lane.down) {
            //stuff happening
            //go upperlane
            this.transform.localScale = upperLaneScale;
            this.transform.position = upperLanePos;
            currentPos_ = Lane.up;
            mr.sortingOrder = 4;
            SetOrder(3);
        }
    }

    private void GoDown() {
        if(currentPos_ == Lane.up) {
            //go lowerLane
            this.transform.localScale = lowerLaneScale;
            this.transform.position = lowerLanePos;
            currentPos_ = Lane.down;
            mr.sortingOrder = 9;
            SetOrder(8);
        }
    }

    private void Dash() {
        if (canDash && !slowdownEffectOn) {
            canDash = false;
            StartCoroutine(Dashing());
        }
    }

    private void SetOrder(int firstOrder) {
        for(int i = 0; i < shade_renderers.Length; i++) {
            shade_renderers[i].sortingOrder = firstOrder - i;
        }
    }

    private IEnumerator Dashing() {
        float time = 0f;
        ParallaxManager.instance_.masterSpeed = 2f;
        skele.AnimationState.SetAnimation(0, "Run_Dash", true);
        shades.SetActive(true);
        charCollider.enabled = false;

        while(time < dashDuration) {
            time += Time.deltaTime;
            //should not collide obstacles (relatives)
            yield return null;
        }

        ParallaxManager.instance_.masterSpeed = 1f;
        skele.AnimationState.SetAnimation(0, "Walk", true);
        charCollider.enabled = true;
        shades.SetActive(false);
        StartCoroutine(DashCooldowner());
    }

    private IEnumerator DashCooldowner() {
        float cooldown = dashcooldown;
        cooldownInd.SetActive(true);
        Image cdi = cooldownInd.GetComponent<Image>();
        while(cooldown >= 0) {
            cooldown -= Time.deltaTime;
            cdi.fillAmount = cooldown / dashcooldown;
            yield return null;
        }
        cdi.fillAmount = 1f;
        cooldownInd.SetActive(false);
        canDash = true;
    }

    private void OnTriggerEnter2D(Collider2D collision) {
        //check what has collided
    }
}
