﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

using TMPro;


public enum WeekDay {
    NONE = 0,
    MONDAY = 1,
    TUESDAY = 2,
    WEDNESDAY = 3,
    THURSDAY = 4,
    FRIDAY = 5
}

public enum TimeOfDay {
    NONE = -1,
    MORNING = 0,
    EVENING = 1
}

public enum DayChangeState {
    NONE = 0,
    WAIT_TIMER = 1,
    ALARM = 2,
    NEW_DAY = 3
}

[System.Serializable]
public class EnemyConfrontation {
    public EnemyType enemyType_;
    public List<Properties> properties_;

    [System.Serializable]
    public class Properties {
        public Sprite sprite;
        public string title;
        public string speechText;
    }


    public Properties GetRandomProperty() {
        return properties_[Random.Range(0, properties_.Count)];
    }
}

public class GameManager : Singleton<GameManager> {

    public List<EnemyConfrontation> enemyConfrontations_;

    public RectTransform titleLogo_;
    public RectTransform dayChangeParent_;

    [Header("Day change states")]
    public RectTransform waitTimerParent_;
    public RectTransform alarmParent_;
    public RectTransform newDayParent_;

    [Header("Death screen")]
    public RectTransform deathScreenParent_;
    public Image deathScreenCharacterImage_;
    public TextMeshProUGUI deathScreenTitleText_;
    public TextMeshProUGUI deathScreenSpeechText_;

    [Header("End scree")]
    public RectTransform endScreenSuccessParent_;
    public RectTransform endScreenFailureParent_;
    public ModifiedButton endScreenSuccessButton_;
    public ModifiedButton endScreenFailureButton_;

    [Header("UI")]
    public ModifiedButton startButton_;
    public ModifiedButton alarmButton_;

    public ModifiedButton deathContinueButton_;

    [Space(10)]
    public TextMeshProUGUI weekDayText_;
    public TextMeshProUGUI timeOfDayText_;

    public WeekDay CurrentDay { get; private set; }
    public DayChangeState CurrentDayChangeState { get; private set; }
    public TimeOfDay CurrentTimeOfDay { get; private set; }

    public float CurrentEnergy { get; private set; }
    public int Lives { get; private set; }

    public bool CanTakeDamage {
        get {
            return CurrentDayChangeState == DayChangeState.NONE;
        }
    }

    private readonly float[] RandomWeekDayWeights = new float[] { 0, 7, 50, 30, 15, 1 };

    private readonly List<string> SceneNames = new List<string>() { "Map", "SideView" };

    private const float ENERGY_LOSS_LOW_ENEMY = 5f;
    

    public EnemyConfrontation GetEnemyConfrontation(EnemyType enemyType) {
        return enemyConfrontations_.Find(x => x.enemyType_ == enemyType);
    }


    public EnemyConfrontation.Properties GetRandomEnemyConfrontationProperty(EnemyType enemyType) {
        EnemyConfrontation enemy = GetEnemyConfrontation(enemyType);

        if (enemy != null) {
            return enemy.GetRandomProperty();
        }

        return null;
    }


    public void NextDay(bool random = false) {
        WeekDay nextDay = CurrentDay;

        if (random) {
            do {
                nextDay = GetRandomWeekDay();
            }
            while (nextDay == CurrentDay);
        }
        else {
            nextDay = (WeekDay)((int)CurrentDay + 1);
        }

        CurrentDay = nextDay;

        Debug.Log("new current day is " + CurrentDay);
    }


    public void SetDay(WeekDay weekDay) {
        CurrentDay = weekDay;
        SetTimeOfDay(TimeOfDay.MORNING);
    }


    public void NextTimeOfDay() {
        if (CurrentTimeOfDay == TimeOfDay.NONE || CurrentTimeOfDay == TimeOfDay.EVENING) {
            SetTimeOfDay(TimeOfDay.MORNING);
        }
        else {
            StartCoroutine(TimeChangeCoroutine());
        }
    }


    public void SetTimeOfDay(TimeOfDay timeOfDay, bool randomDay = false, bool overrideDay = false) {
        if ((CurrentTimeOfDay == TimeOfDay.EVENING && timeOfDay == TimeOfDay.MORNING) || overrideDay) {
            ChangeDay(randomDay);
        }
        else if (CurrentTimeOfDay == TimeOfDay.NONE) {
            ChangeDay();
        }

        CurrentTimeOfDay = timeOfDay;
        timeOfDayText_.text = CurrentTimeOfDay.ToString();

        if (CurrentTimeOfDay == TimeOfDay.MORNING) {
            MapGeneralManager.instance_.SetCheckpointType(
                CheckpointType.WORK,
                (randomDay) ? CheckpointType.RANDOM : CheckpointType.HOME
            );
        }
        else if (CurrentTimeOfDay == TimeOfDay.EVENING) {
            MapGeneralManager.instance_.SetCheckpointType(
                CheckpointType.HOME,
                (randomDay) ? CheckpointType.RANDOM : CheckpointType.WORK
            );
        }
    }


    public void ChangeDay(bool randomDay = false, WeekDay weekDay = WeekDay.NONE) {
        if (CurrentDay == WeekDay.FRIDAY && !randomDay) {
            // victory!
            EndGame(true);

            return;
        }

        if (randomDay) {
            CurrentEnergy = 50f;
        }
        else {
            CurrentEnergy = 100f;
        }

        StartCoroutine(DayChangeCoroutine(randomDay, weekDay));
    }


    public void EnemyHit(EnemyType enemy) {
        if (!CanTakeDamage) {
            return;
        }

        if (enemy == EnemyType.LOW) {
            CurrentEnergy -= ENERGY_LOSS_LOW_ENEMY;
        }
        else {
            Lives--;
            MapGeneralManager.instance_.ClearEnemies();
            StartCoroutine(EnemyHitCoroutine(enemy));
        }
    }


    private void StartGame() {
        titleLogo_.gameObject.SetActive(false);
        startButton_.gameObject.SetActive(false);

        Lives = 3;

        StartCoroutine(StartGameCoroutine());
        // LOAD SCENES
    }


    private IEnumerator StartGameCoroutine() {
        foreach (string sceneName in SceneNames) {
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
        }

        yield return new WaitUntil(() => MapGeneralManager.instance_ != null);
        yield return null;

        CurrentDay = WeekDay.NONE;
        CurrentTimeOfDay = TimeOfDay.NONE;
        SetDayChangeState(DayChangeState.NONE);

        SetTimeOfDay(TimeOfDay.MORNING);
    }


    private void EndGame(bool success) {
        titleLogo_.gameObject.SetActive(true);
        startButton_.gameObject.SetActive(true);

        CurrentDay = WeekDay.NONE;
        CurrentTimeOfDay = TimeOfDay.NONE;
        SetDayChangeState(DayChangeState.NONE);

        foreach (string sceneName in SceneNames) {
            SceneManager.UnloadSceneAsync(sceneName);
        }

        if (success) {
            endScreenSuccessParent_.gameObject.SetActive(true);
        }
        else {
            endScreenFailureParent_.gameObject.SetActive(true);
        }

        // UNLOAD SCENES
    }


    private void SetDayChangeState(DayChangeState nextState) {
        CurrentDayChangeState = nextState;

        dayChangeParent_.gameObject.SetActive(CurrentDayChangeState != DayChangeState.NONE);

        waitTimerParent_.gameObject.SetActive(CurrentDayChangeState == DayChangeState.WAIT_TIMER);
        alarmParent_.gameObject.SetActive(CurrentDayChangeState == DayChangeState.ALARM);
        newDayParent_.gameObject.SetActive(CurrentDayChangeState == DayChangeState.NEW_DAY);

        if (CurrentDayChangeState == DayChangeState.NEW_DAY) {
            weekDayText_.text = CurrentDay.ToString();            
        }
    }


    private IEnumerator EnemyHitCoroutine(EnemyType enemyType) {
        if (Lives == 0) {
            EndGame(false);
            yield break;
        }

        EnemyConfrontation.Properties properties = GetRandomEnemyConfrontationProperty(enemyType);

        deathScreenTitleText_.text = properties.title;
        deathScreenSpeechText_.text = properties.speechText;
        deathScreenCharacterImage_.sprite = properties.sprite;

        deathScreenParent_.gameObject.SetActive(true);

        bool clicked = false;

        UnityEngine.Events.UnityAction buttonAction = () => {
            clicked = true;

            deathContinueButton_.onClick.RemoveAllListeners();
        };
        deathContinueButton_.onClick.AddListener(buttonAction);

        yield return new WaitUntil(() => clicked);

        deathScreenParent_.gameObject.SetActive(false);

        if (enemyType == EnemyType.MEDIUM) {
            //ChangeDay(true);
            SetTimeOfDay(TimeOfDay.MORNING, true, true);
        }
        else if (enemyType == EnemyType.HIGH) {
            ChangeDay(weekDay: WeekDay.MONDAY);
        }
    }


    private IEnumerator DayChangeCoroutine(bool randomDay, WeekDay weekDay = WeekDay.NONE) {
        SetDayChangeState(DayChangeState.WAIT_TIMER);

        yield return new WaitForSeconds(3);

        SetDayChangeState(DayChangeState.ALARM);

        yield return new WaitForSeconds(2);

        if (weekDay == WeekDay.NONE) {
            Debug.Log("next day");
            NextDay(randomDay);
        }
        else {
            SetDay(weekDay);
        }

        SetDayChangeState(DayChangeState.NEW_DAY);

        yield return new WaitForSeconds(2);

        SetDayChangeState(DayChangeState.NONE);
    }


    private IEnumerator TimeChangeCoroutine() {
        SetDayChangeState(DayChangeState.WAIT_TIMER);

        yield return new WaitForSeconds(3);

        SetDayChangeState(DayChangeState.ALARM);

        yield return new WaitForSeconds(2);

        SetTimeOfDay(TimeOfDay.EVENING);

        SetDayChangeState(DayChangeState.NEW_DAY);

        yield return new WaitForSeconds(2);

        SetDayChangeState(DayChangeState.NONE);
    }


    private WeekDay GetRandomWeekDay() {
        return (WeekDay)Util.WeightedRandom.GetWeightedRandomIndex(RandomWeekDayWeights);
    }


    public override void Initialize() {
        base.Initialize();

        startButton_.onClick.AddListener(StartGame);

        endScreenSuccessButton_.onClick.AddListener(() => { endScreenSuccessParent_.gameObject.SetActive(false); });
        endScreenFailureButton_.onClick.AddListener(() => { endScreenFailureParent_.gameObject.SetActive(false); });

        endScreenSuccessParent_.gameObject.SetActive(false);
        endScreenFailureParent_.gameObject.SetActive(false);

        deathScreenParent_.gameObject.SetActive(false);
        titleLogo_.gameObject.SetActive(true);

        CurrentDay = WeekDay.NONE;
        CurrentTimeOfDay = TimeOfDay.NONE;
        SetDayChangeState(DayChangeState.NONE);
    }


    protected override void Awake() {
        base.Awake();

        Application.targetFrameRate = 60;

        Initialize();
    }


    private void Update() {
        if (Input.GetKeyDown(KeyCode.N)) {
            EndGame(true);
        }
    }
}
