﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;

using TMPro;

public enum CheckpointType {
    NONE = 0,
    HOME = 1,
    WORK = 2,
    RANDOM = 3
}


[RequireComponent(typeof(SphereCollider))]
public class MapCheckpoint : MonoBehaviour {

    public CheckpointType myCheckpointType_;
    public ParticleSystem particleSystem_;
    public TextMeshPro nameText;

    private SphereCollider collider_;


    public void Initialize(CheckpointType checkpointType, Vector3 position) {
        myCheckpointType_ = checkpointType;
        transform.position = position;

        particleSystem_.gameObject.SetActive(myCheckpointType_ == MapGeneralManager.instance_.CurrentTargetCheckpointType);
        nameText.text = myCheckpointType_.ToString();
    }


    private void OnTriggerEnter(Collider other) {        
        if (other.GetComponent<MapPlayerController>() != null) {
            if (myCheckpointType_ == MapGeneralManager.instance_.CurrentTargetCheckpointType) {
                GameManager.instance_.NextTimeOfDay();
                MapGeneralManager.instance_.ClearEnemies();
            }
        }
    }


    private void Awake() {
        collider_ = GetComponent<SphereCollider>();

        if (!collider_.isTrigger) {
            collider_.isTrigger = false;
        }
    }
}
