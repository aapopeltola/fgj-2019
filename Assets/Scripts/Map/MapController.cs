﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;


public class MapController : Singleton<MapController> {

    public Transform checkpointParent_;

    public List<Transform> homeCheckpointPositions_;
    public List<Transform> workCheckpointPositions_;
    public List<Transform> randomCheckpointPositions_;

    public MapCheckpoint HomeCheckpoint { get; private set; }
    public MapCheckpoint WorkCheckpoint { get; private set; }


    private static string CHECPOINT_PREFAB_PATH = "Prefabs/Map/prefab_checkpoint";


    public Vector3 GetSpawnPosition(CheckpointType type) {
        Vector3 output = new Vector3();

        switch(type) {
            case CheckpointType.HOME: {
                output = HomeCheckpoint.transform.position;
                break;
            }
            case CheckpointType.WORK: {
                output = WorkCheckpoint.transform.position;
                break;
            }
            case CheckpointType.RANDOM: {
                output = randomCheckpointPositions_[Random.Range(0, randomCheckpointPositions_.Count)].position;
                break;
            }
        }

        return output;
    }



    public override void Initialize() {
        base.Initialize();

        name = "Map";

        Transform homePosition = homeCheckpointPositions_[Random.Range(0, homeCheckpointPositions_.Count)];
        Transform workPosition = workCheckpointPositions_[Random.Range(0, workCheckpointPositions_.Count)];

        GameObject checkpointPrefab = Resources.Load<GameObject>(CHECPOINT_PREFAB_PATH);

        HomeCheckpoint = Instantiate(checkpointPrefab).GetComponent<MapCheckpoint>();
        HomeCheckpoint.Initialize(CheckpointType.HOME, homePosition.position);

        WorkCheckpoint = Instantiate(checkpointPrefab).GetComponent<MapCheckpoint>();
        WorkCheckpoint.Initialize(CheckpointType.WORK, workPosition.position);

        HomeCheckpoint.transform.SetParent(checkpointParent_);
        WorkCheckpoint.transform.SetParent(checkpointParent_);

        /*foreach (NavMeshSurface navMesh in NavMeshSurface.activeSurfaces) {
            navMesh.BuildNavMesh();
        }*/
    }
}
