﻿using UnityEngine;

using System.Linq;
using System.Collections;
using System.Collections.Generic;


public enum Priority {
    LOW = 0,
    MEDIUM = 1,
    HIGH = 2,
}

public class Shake {
    public float duration_;
    public float amount_;
    public Priority priority_;

    private float timer_;


    public float TimeLeft {
        get {
            return duration_ - timer_;
        }
    }


    public Shake(float duration, float amount, Priority priority) {
        this.duration_ = duration;
        this.amount_ = amount;
        this.priority_ = priority;
    }


    public bool Update() {
        timer_ += Time.unscaledDeltaTime;

        if (timer_ < duration_) {
            return true;
        }
        else {
            return false;
        }
    }
}


public class MapCameraController : Singleton<MapCameraController> {
    [Header("Basic camera settings")]
    public float cameraDistance = 40f;
    public float cameraMovementSpeed = 2f;
    
    [Header("Parents")]
    public Camera camera_;

    [Header("Shake")]
    public Transform mainCameraShakeTransform_;
    public float cameraShakeLerpSpeed_ = 1f;
    public float cameraShakeSinTime_ = 10f;

    private Coroutine dynamicCameraPositioning_;

    private List<Shake> shakeList_ = new List<Shake>();
    private Coroutine shakeCoroutine_ = null;
    private Coroutine resetCameraShake_;

    private bool keepShaking;
    

    public void ResetCameraPositioning() {
        if (dynamicCameraPositioning_ != null) {
            StopCoroutine(dynamicCameraPositioning_);
        }
        dynamicCameraPositioning_ = StartCoroutine(DynamicCameraPositioning());
    }


    public void AddShake(float duration, float amount, Priority priority = Priority.LOW) {
        shakeList_.Add(new Shake(duration, amount, priority));

        if (shakeCoroutine_ == null) {
            if (resetCameraShake_ != null) {
                StopCoroutine(resetCameraShake_);
                resetCameraShake_ = null;
            }
            shakeCoroutine_ = StartCoroutine(CameraShake());
        }
    }


    private IEnumerator DynamicCameraPositioning() {
        yield return new WaitUntil(() => MapPlayerController.instance_ != null);

        Vector3 primaryCameraVelocity = new Vector3();
        List<Vector3> playerPositions = new List<Vector3>();

        while (true) {
            camera_.transform.position = Vector3.SmoothDamp(
                camera_.transform.position,
                SingleTargetCameraPosition(MapPlayerController.instance_.transform.position),
                ref primaryCameraVelocity,
                Time.fixedDeltaTime * cameraMovementSpeed
            );

            yield return new WaitForFixedUpdate();
        }
    }


    private Vector3 SingleTargetCameraPosition(Vector3 target) {
        return new Vector3 (
            target.x,
            target.y + cameraDistance,
            target.z
        );
    }



    private IEnumerator CameraShake() {
        float amount = 0f, lowPriorityAmount = 0f, mediumPriorityAmount = 0f, highPriorityAmount = 0f, lastShakeAmount = 0f;
        float timeToShake = 0f;
        float shakeOffTime = 0f;

        List<Shake> removedShakes = new List<Shake>();

        keepShaking = false;

        Vector3 shakeOffset = Vector3.zero;

        do {
            for (int i = 0; i < shakeList_.Count; i++) {
                if (shakeList_[i].Update()) {
                    if (shakeList_[i].priority_ == Priority.LOW) {
                        lowPriorityAmount += shakeList_[i].amount_;
                    }
                    else if (shakeList_[i].priority_ == Priority.MEDIUM) {
                        mediumPriorityAmount += shakeList_[i].amount_;
                    }
                    else if (shakeList_[i].priority_ == Priority.HIGH) {
                        highPriorityAmount += shakeList_[i].amount_;
                    }
                }
                else {
                    removedShakes.Add(shakeList_[i]);
                }
            }

            // The shake amount is by the highest priority
            if (highPriorityAmount > 0) {
                amount = highPriorityAmount;
            }
            else if (mediumPriorityAmount > 0) {
                amount = mediumPriorityAmount;
            }
            else if (lowPriorityAmount > 0){
                amount = lowPriorityAmount;
            }

            if (shakeList_.Count > 0) {
                shakeOffTime = GetShakeOffTime();
                if (amount >= 0) {
                    lastShakeAmount = amount;
                }
            }

            shakeList_.RemoveAll(x => removedShakes.Contains(x));

            // If all shakes are removed, check how long we will keep shaking. Otherwise we just shake
            if (shakeList_.Count == 0 && !keepShaking) {
                keepShaking = true;
                timeToShake = shakeOffTime;
            }

            // If time to shake is less than zero, don't keep shaking
            if (shakeList_.Count == 0 && timeToShake < 0) {
                keepShaking = false;
            }
            else if (shakeList_.Count == 0 && timeToShake > 0) {
                timeToShake -= Time.unscaledDeltaTime;
            }

            if (shakeList_.Count == 0) {
                amount = lastShakeAmount * (timeToShake / shakeOffTime);
            }

            shakeOffset = new Vector3(
                Random.Range(Mathf.Sin(Time.time * -cameraShakeSinTime_), Mathf.Sin(Time.time * cameraShakeSinTime_)) * amount,
                Random.Range(Mathf.Sin(Time.time * -cameraShakeSinTime_), Mathf.Sin(Time.time * cameraShakeSinTime_)) * amount
            );

            mainCameraShakeTransform_.localPosition = Vector3.Lerp(
                mainCameraShakeTransform_.localPosition, 
                shakeOffset, 
                Time.unscaledDeltaTime * cameraShakeLerpSpeed_
            );

            if (shakeList_.Count > 0) {
                amount = -1f;
                lowPriorityAmount = 0f;
                mediumPriorityAmount = 0f;
                highPriorityAmount = 0f;
            }

            removedShakes.Clear();
            yield return null;
        }
        while (shakeList_.Count > 0 || keepShaking);

        resetCameraShake_ = StartCoroutine(ResetCameraShake());

        shakeCoroutine_ = null;
    }


    private IEnumerator ResetCameraShake() {
        yield return Util.Transitions.Move(
            0.25f,
            mainCameraShakeTransform_,
            mainCameraShakeTransform_.localPosition,
            Vector3.zero,
            MovementSpace.LOCAL
        );

        resetCameraShake_ = null;
    }


    private float GetShakeOffTime() {
        return new List<Shake>(shakeList_).OrderByDescending(x => x.duration_).ToList()[0].duration_;
    }
}
