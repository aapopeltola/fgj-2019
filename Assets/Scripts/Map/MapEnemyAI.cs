﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;


public enum EnemyType {
    LOW = 0,
    MEDIUM = 1,
    HIGH = 2
}

public enum State {
    IDLE = 0, 
    FIND_TARGET = 1,
    FOLLOW = 2
}


[RequireComponent(typeof(NavMeshAgent))]
public class MapEnemyAI : MonoBehaviour {

    public EnemyType enemyType_;

    public float followDistance_;
    public float catchDistance_ = 10f;
    
    private NavMeshAgent navMeshAgent_;

    private State myState_;

    private float timeSinceLastDistanceCheck_;
    private float currentDistanceToPlayer_;

    private const float CHECK_DISTANCE_FREQUENCY = 1f;

    private int layerMask_ = 1 << 19 | 1 << 20;
    

    public void OnSpawn(Vector3 position) {
        transform.position = position;
    }


    private bool CheckDistanceToPlayer() {
        currentDistanceToPlayer_ = Vector3.Distance(transform.position, MapPlayerController.instance_.GetPosition());

        if (myState_ == State.IDLE) {
            if (currentDistanceToPlayer_ <= followDistance_) {
                SetState(State.FIND_TARGET);

                return true;
            }
        }
        else if (myState_ == State.FOLLOW) {
            if (currentDistanceToPlayer_ <= catchDistance_) {
                MapGeneralManager.instance_.EnemyHit(this);

                return true;
            }
        }

        return false;
    }


    private void CheckLineOfSight() {
        RaycastHit[] raycastHits = Physics.RaycastAll(
            origin: transform.position + Vector3.up,
            direction: (MapPlayerController.instance_.GetPosition() - transform.position),
            maxDistance: Vector3.Distance(MapPlayerController.instance_.GetPosition(), transform.position),
            layerMask: layerMask_
        );
        
        if (raycastHits.Length == 0) {
            SetState(State.FOLLOW);
        }
    }


    private void SetState(State state) {
        myState_ = state;
    }


    private void Awake() {
        navMeshAgent_ = GetComponent<NavMeshAgent>();

        myState_ = State.IDLE;
    }


    private void Update() {
        if (timeSinceLastDistanceCheck_ >= CHECK_DISTANCE_FREQUENCY) {
            timeSinceLastDistanceCheck_ = 0f;

            if (myState_ == State.IDLE) {
                CheckDistanceToPlayer();
                return;
            }
            else if (myState_ == State.FIND_TARGET) {
                CheckLineOfSight();
                return;
            }
            else if (myState_ == State.FOLLOW) {
                if (CheckDistanceToPlayer()) {
                    return;
                }
            }
        }
        else {
            timeSinceLastDistanceCheck_ += Time.deltaTime;
        }

        if (myState_ == State.FOLLOW) {
            navMeshAgent_.SetDestination(MapPlayerController.instance_.GetPosition());
        }
    }
}
