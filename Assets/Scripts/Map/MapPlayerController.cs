﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class MapPlayerController : Singleton<MapPlayerController> {

    public float defaultMovementSpeed_ = 1f;

    public Transform pathEffect_;

    public Canvas canvas_;
    public RectTransform joystickPosition_;
    public RectTransform joystickDirectionIndicator_;

    public RectTransform directionArrow_;

    public AnimationCurve joystickMovementSpeed_;

    public float joystickFollowSpeed_ = 0.5f;

    private float CurrentMovementSpeed {
        get {
            float multiplier = 1f;
            if (ParallaxManager.instance_ != null) {
                multiplier = ParallaxManager.instance_.masterSpeed;
            }
            return defaultMovementSpeed_ * multiplier;
        }
    }

    private float movementVerticalInput_;
    private float movementHorizontalInput_;

    private Vector3 velocity_;
    private Vector3 previousPosition_;
    private Vector3 translateDirection_;

    private CanvasScaler canvasScaler_;

    private float distance_;
    private const float MAX_DISTANCE = 200f;


    public void Initialize(Vector3 spawnPosition) {
        base.Initialize();

        name = "Player";

        SetPosition(spawnPosition);
    }


    public void SetPosition(Vector3 position) {
        pathEffect_.gameObject.SetActive(false);
        transform.position = position;
        pathEffect_.gameObject.SetActive(true);
    }


    public Vector3 GetPosition() {
        return transform.position;
    }


    public Vector3 GetDirection() {
        return translateDirection_;
    }


    private void ProcessInput(bool interpolate = true) {
        Vector2 inputPosition = canvas_.worldCamera.ScreenToViewportPoint(Input.touches[0].position);
        Vector2 targetPosition = canvasScaler_.referenceResolution * inputPosition;
        Vector2 inputDirection = (targetPosition - joystickPosition_.anchoredPosition).normalized;

        distance_ = Mathf.Clamp(Vector3.Distance(targetPosition, joystickPosition_.anchoredPosition), 0, MAX_DISTANCE);

        if (interpolate) {
            joystickPosition_.anchoredPosition = Vector3.Lerp(
                joystickPosition_.anchoredPosition,
                targetPosition,
                Time.deltaTime * (joystickFollowSpeed_ * (1 + (joystickMovementSpeed_.Evaluate(distance_ / MAX_DISTANCE) * 50f)))
            );
        }
        else {
            joystickPosition_.anchoredPosition = targetPosition;
        }

        joystickDirectionIndicator_.anchoredPosition = inputDirection * distance_;

        movementHorizontalInput_ = inputDirection.x;
        movementVerticalInput_ = inputDirection.y; 
    }


    private void ProcessMovement() {
        if (movementVerticalInput_ == 0 && movementHorizontalInput_ == 0) {
            return;
        }

        if (movementVerticalInput_ != 0 || movementHorizontalInput_ != 0) {
            velocity_ = previousPosition_ - transform.position;
            previousPosition_ = transform.position;
        }
        else {
            velocity_ = Vector3.zero;
            previousPosition_ = Vector3.zero;
        }

        translateDirection_ = new Vector3(movementHorizontalInput_, 0, movementVerticalInput_);

        float normalizedValue = (distance_ / MAX_DISTANCE);
        float movementSpeed = CurrentMovementSpeed * ((normalizedValue < 0.2f) ? 0 : 1);

        Vector3 aimDirection = new Vector3(
            transform.eulerAngles.x,
            Mathf.Atan2(movementHorizontalInput_, movementVerticalInput_) * Mathf.Rad2Deg,
            transform.eulerAngles.z
        );
        transform.rotation = Quaternion.AngleAxis(aimDirection.y, Vector3.up);

        MapCheckpoint mapCheckpoint = MapGeneralManager.instance_.GetTargetCheckpoint();
        Vector3 targetDirection = Vector3.Normalize(
            mapCheckpoint.transform.position - transform.position
        );

        Vector3 arrowDirection = new Vector3(
            transform.eulerAngles.x,
            Mathf.Atan2(targetDirection.x, targetDirection.z) * Mathf.Rad2Deg,
            transform.eulerAngles.z
        );

        directionArrow_.localEulerAngles = new Vector3(0, 0, -arrowDirection.y);
        
        transform.position = Vector3.Lerp(
            transform.position,
            Vector3.MoveTowards(
                transform.position,
                transform.position + translateDirection_,
                Time.fixedDeltaTime * movementSpeed
            ),
            Time.fixedDeltaTime * movementSpeed
        );
    }


    protected override void Awake() {
        base.Awake();

        canvas_.worldCamera = MapCameraController.instance_.camera_;
        canvasScaler_ = canvas_.GetComponent<CanvasScaler>();

        joystickPosition_.gameObject.SetActive(false);
    }


    private void FixedUpdate() {
        ProcessMovement();       
    }


    private void Update() {
        if (Input.touchCount == 0) {
            joystickPosition_.gameObject.SetActive(false);
            movementHorizontalInput_ = 0f;
            movementVerticalInput_ = 0f;
            return;
        }
        else {
            if (!joystickPosition_.gameObject.activeSelf) {
                joystickPosition_.gameObject.SetActive(true);

                Vector2 inputPosition = canvas_.worldCamera.ScreenToViewportPoint(Input.touches[0].position);
                Vector2 targetPosition = canvasScaler_.referenceResolution * inputPosition;

                joystickPosition_.anchoredPosition = targetPosition;
            }
            ProcessInput();
        }

        //if (Input.GetMouseButtonDown(0)) {
        //    ProcessInput(false);
        //    joystickPosition_.gameObject.SetActive(true);
        //}
        //else if (Input.GetMouseButtonUp(0)) {
        //    joystickPosition_.gameObject.SetActive(false);
        //}
        //else if (Input.GetMouseButton(0)) {
        //    ProcessInput();
        //}
        //else {
        //    movementHorizontalInput_ = 0f;
        //    movementVerticalInput_ = 0f;
        //}
    }
}
