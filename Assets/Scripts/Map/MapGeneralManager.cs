﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.AI;


public class MapGeneralManager : Singleton<MapGeneralManager> {

    public delegate void EnemyEvent(EnemyType enemyType);
    public static event EnemyEvent OnEnemySpawned;
    public static event EnemyEvent OnEnemyTriggered;

    public Transform playerParent;
    public Transform enemyParent;

    public CheckpointType CurrentTargetCheckpointType { get; private set; }
    public CheckpointType CurrentSpawnCheckpointType { get; private set; }

    private static string PLAYER_CHARACTER_PREFAB_PATH = "Prefabs/Map/prefab_playerCharacter";
    private static string ENEMY_LOW_CHARACTER_PREFAB_PATH = "Prefabs/Map/prefab_enemyCharacter_LOW";
    private static string ENEMY_HIGH_CHARACTER_PREFAB_PATH = "Prefabs/Map/prefab_enemyCharacter_HIGH";

    private GameObject enemyLowPrefab_;
    private GameObject enemyHighPrefab_;

    private Vector3 previousEnemySpawnPlayerPosition_;
    private float timeSinceLastEnemySpawn_;

    private List<MapEnemyAI> mapEnemyAIList_ = new List<MapEnemyAI>();

    private const float ENEMY_SPAWN_FREQUENCY_DISTANCE = 40f;
    private const float ENEMY_STAY_MAX_DISTANCE = 200f;
    private const float ENEMY_SPAWN_CHECK_FREQUENCY = 2f;
    private const float ENEMY_SPAWN_DISTANCE = 200f;


    public override void Initialize() {        
        base.Initialize();
        
        MapController.instance_.Initialize();

        GameObject playerPrefab = Resources.Load<GameObject>(PLAYER_CHARACTER_PREFAB_PATH);
        if (playerPrefab != null)  {
            GameObject playerObject = Instantiate(playerPrefab);
            playerObject.transform.SetParent(playerParent);

            MapPlayerController.instance_.Initialize(MapController.instance_.GetSpawnPosition(CheckpointType.HOME));
        }

        MapCameraController.instance_.ResetCameraPositioning();
    }


    public void SetCheckpointType(CheckpointType target, CheckpointType spawn) {
        CurrentTargetCheckpointType = target;
        CurrentSpawnCheckpointType = spawn;

        Debug.Log("SetCheckpointType " + target + ", " + spawn);

        MapPlayerController.instance_.SetPosition(MapController.instance_.GetSpawnPosition(CurrentSpawnCheckpointType));

        ClearEnemies();
    }


    public void ClearEnemies() {
        for (int i = 0; i < mapEnemyAIList_.Count; i++) {
            Destroy(mapEnemyAIList_[i].gameObject);
        }
        mapEnemyAIList_.Clear();
        previousEnemySpawnPlayerPosition_ = MapPlayerController.instance_.transform.position;
    }


    public MapCheckpoint GetTargetCheckpoint() {
        if (CurrentTargetCheckpointType == CheckpointType.HOME) {
            return MapController.instance_.HomeCheckpoint;
        }
        else if (CurrentTargetCheckpointType == CheckpointType.WORK) {
            return MapController.instance_.WorkCheckpoint;
        }

        return null;
    }


    public void EnemyHit(MapEnemyAI enemy) {
        OnEnemyTriggered?.Invoke(enemy.enemyType_);
        mapEnemyAIList_.Remove(enemy);
        Destroy(enemy.gameObject);
    }


    private void SpawnEnemies() {
        timeSinceLastEnemySpawn_ = 0;

        Vector3 playerPosition = MapPlayerController.instance_.GetPosition();
        float lastSpawnDistance = Vector3.Distance(playerPosition, previousEnemySpawnPlayerPosition_);

        if (lastSpawnDistance >= ENEMY_SPAWN_FREQUENCY_DISTANCE) {
            List<MapEnemyAI> tooFarAwayEnemies = new List<MapEnemyAI>();

            for (int i = 0; i < mapEnemyAIList_.Count; i++) {
                float distance = Vector3.Distance(mapEnemyAIList_[i].transform.position, playerPosition);

                if (distance > ENEMY_STAY_MAX_DISTANCE) {
                    tooFarAwayEnemies.Add(mapEnemyAIList_[i]);
                }
            }

            Vector3 direction = MapPlayerController.instance_.GetDirection();
            
            if (direction.magnitude < 0.1f) {
                direction = Random.onUnitSphere;
            }

            Vector3 spawnPositionCenter = playerPosition + direction * ENEMY_SPAWN_DISTANCE;
            
            for (int i = 0; i < 5; i++) {
                Vector3 spawnPosition = Util.General.GetRandomPositionOnUnitSphere(spawnPositionCenter, 100f, true);

                if (tooFarAwayEnemies.Count > 0) {
                    tooFarAwayEnemies[0].OnSpawn(spawnPosition);
                    tooFarAwayEnemies.RemoveAt(0);
                }
                else {
                    int randomValue = Random.Range(0, 100);
                    GameObject prefab = (randomValue > 25) ? enemyLowPrefab_ : enemyHighPrefab_;
                    
                    GameObject newEnemyGameObject = Instantiate(prefab);
                    newEnemyGameObject.transform.SetParent(enemyParent);

                    MapEnemyAI enemy = newEnemyGameObject.GetComponent<MapEnemyAI>();
                    enemy.OnSpawn(spawnPosition);

                    mapEnemyAIList_.Add(enemy);
                }
            }

            previousEnemySpawnPlayerPosition_ = playerPosition;
        }
    }


    protected override void Awake() {
        base.Awake();

        enemyLowPrefab_ = Resources.Load<GameObject>(ENEMY_LOW_CHARACTER_PREFAB_PATH);
        enemyHighPrefab_ = Resources.Load<GameObject>(ENEMY_HIGH_CHARACTER_PREFAB_PATH);

        previousEnemySpawnPlayerPosition_ = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

        Initialize();
    }


    private void Update() {
        if (timeSinceLastEnemySpawn_ >= ENEMY_SPAWN_CHECK_FREQUENCY) {
            SpawnEnemies();
        }
        else {
            timeSinceLastEnemySpawn_ += Time.deltaTime;
        }
    }
}
