﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

public class ModifiedButton : Button {

    private static int disableCallCount_ = 0;

    public static bool isEnabled_ {
        get {
            return disableCallCount_ == 0;
        }
    }


    public override void OnPointerClick(UnityEngine.EventSystems.PointerEventData eventData) {
        if(!isEnabled_) {
            return;
        }

        base.OnPointerClick(eventData);
    }


    public override void OnSubmit(UnityEngine.EventSystems.BaseEventData eventData) {
        if(!isEnabled_) {
            return;
        }
        
        base.OnSubmit(eventData);
    }


    public override void OnPointerDown(UnityEngine.EventSystems.PointerEventData eventData) {
        if(!isEnabled_) {
            return;
        }

        base.OnPointerDown(eventData);
    }


    public override void OnPointerUp(UnityEngine.EventSystems.PointerEventData eventData) {
        if(!isEnabled_) {
            return;
        }

        base.OnPointerUp(eventData);
    }


    public static void SetEnabled() {
        //isEnabled_ = true;
        disableCallCount_ = Mathf.Clamp(disableCallCount_ - 1, 0, disableCallCount_);
    }


    public static void SetDisabled() {
        //isEnabled_ = false;
        disableCallCount_++;
    }
}
