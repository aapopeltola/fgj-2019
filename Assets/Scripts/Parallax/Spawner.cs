﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Spawner : MonoBehaviour
{
    public GameObject[] spawnlist;
    public GameObject endPoint;
    public float successPercent = 0.5f;
    public float speed;
    public float waitTime = 3f;
    public Vector2 dir;
    public bool masterSpeedAffects;

    protected bool canSpawn_ = true;

    // Update is called once per frame
    protected virtual void Update()
    {
        
    }

    protected virtual IEnumerator SpawnOrNot(GameObject mob) {
        if (Random.Range(0f, 1f) < successPercent) {
            GameObject go = Instantiate<GameObject>(mob, this.transform);
            go.transform.position = this.transform.position;

            while (go.transform.position.x > (endPoint.transform.position.x + 1f) ||
                go.transform.position.x < (endPoint.transform.position.x - 1f)) {
                go.transform.position += (new Vector3(speed *
                    (masterSpeedAffects ? ParallaxManager.instance_.masterSpeed : 1f), 0, 0) * dir.x);
                yield return null;
            }
            Destroy(go);
        }

        StartCoroutine(WaitTime());
    }

    protected virtual IEnumerator WaitTime() {
        float time = 0f;
        while (time < waitTime) {
            time += Time.deltaTime;
            yield return null;
        }
        canSpawn_ = true;
    }
}
