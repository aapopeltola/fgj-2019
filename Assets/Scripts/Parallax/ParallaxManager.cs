﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public enum ParallaxType {
    farBackGround,
    BackGround,
    foreGround,
    Skyline,
    RoadPieces
}

public class ParallaxManager : Singleton<ParallaxManager>
{
    public Camera sideCam;
    public GameObject parallaxParent;

    [Header("Values For All")]
    public float masterSpeed;
    public Vector2 moveDir;
    public float midSectionThresh;
    public Dictionary<ParallaxType, GameObject[]> parallaxObjs = new Dictionary<ParallaxType, GameObject[]>();
    /// Background x 2
    public GameObject[] farBackground;

    /// Foreground x 2
    public GameObject[] foreGround;
    public GameObject[] nearCamera;
    /// Actor sheet. This could be texture offset?
    
    /// Props (non functional)
    public GameObject[] nonF_props;
    /// Props (functional --> jumpable
    public GameObject[] f_props;

    [Header("NearBackGroud")]
    public float nearBgSpeed;
    public Vector3 nearBgStartPos;  

    [Header("RoadPieces")]
    public float roadSpeed;
    public Vector3 roadStartPos;

    [Header("Skyline")]
    public float skylineSpeed;
    public Vector3 skylineStartPos;

    public List<GameObject> roadPieces = new List<GameObject>();
    public List<GameObject> backgroundPieces = new List<GameObject>();
    public List<GameObject> skylinePieces = new List<GameObject>();

    public MobSpawner[] mobSpawners;

    public delegate void OnCatch(EnemyType enemytype);
    public static event OnCatch OnEnemyCatchedMe;

    private ParallaxPiece pp;

    protected override void Awake() {
        base.Awake();
    }

    // Start is called before the first frame update
    void Start()
    {
        MapGeneralManager.OnEnemyTriggered += SpawnMob;
        LoadPrefabs();

        NextPiece(ParallaxType.RoadPieces, this);
        NextPiece(ParallaxType.BackGround, this);
        NextPiece(ParallaxType.Skyline, this);
    }

    private void SpawnMob(EnemyType enemyType) {
        if(UnityEngine.Random.Range(0f,1f) > 0.5f) {
            mobSpawners[0].SpawnMob(enemyType);
        }
        else {
            mobSpawners[1].SpawnMob(enemyType);
        }
    }

    public void EnemyCathedMe(EnemyType mobType) {
        //Debug.Log(mobType.ToString() + " catched me");
        ParallaxManager.OnEnemyCatchedMe?.Invoke(mobType);

        GameManager.instance_.EnemyHit(mobType);
    }


    public void NextPiece<T>(ParallaxType piece, T caller) {
        switch (piece) {
            case ParallaxType.foreGround:
                break;
            case ParallaxType.BackGround:
                if (backgroundPieces.Count <= 4) {
                    InstantiateParallaxObject(parallaxObjs[piece][Random.Range(0, parallaxObjs[piece].Length)], caller, GetStartPos(piece), GetSpeed(piece));
                }
                else {
                    PickObjectFromPool(piece, caller, ref backgroundPieces);
                }
                break;
            case ParallaxType.Skyline:
                if (skylinePieces.Count <= 1) {
                    InstantiateParallaxObject(parallaxObjs[piece][Random.Range(0, parallaxObjs[piece].Length)], caller, GetStartPos(piece), GetSpeed(piece));
                }
                else {
                    PickObjectFromPool(piece, caller, ref skylinePieces);
                }
                break;
            case ParallaxType.RoadPieces:
                if (roadPieces.Count <= 0) {
                    InstantiateParallaxObject(parallaxObjs[piece][0], caller, GetStartPos(piece), GetSpeed(piece));
                }
                else {
                    PickObjectFromPool(piece, caller, ref roadPieces);
                }
                break;
            default:
                break;

        }
    }

    private void InstantiateParallaxObject<T>(GameObject prefab, T caller, Vector3 roadStartPos, float speed) {
        GameObject parGo = Instantiate<GameObject>(prefab, parallaxParent.transform);
        pp = parGo.GetComponent<ParallaxPiece>();
        
        if (caller is ParallaxPiece) {
            pp.ReadyUp((caller as ParallaxPiece).ReturnFollowerPosition(), moveDir, speed);
            
        }
        else if(caller is ParallaxManager) {
            pp.ReadyUp(roadStartPos, moveDir, speed);
        }
        else {
            Debug.Log("Pls don't. Just don't");
        }
    }

    public float GetSpeed(ParallaxType piece) {
        switch (piece) {
            case ParallaxType.foreGround:
                return 0;
            case ParallaxType.BackGround:
                return nearBgSpeed;
            case ParallaxType.Skyline:
                return skylineSpeed;
            case ParallaxType.RoadPieces:
                return roadSpeed;
            default:
                return 0;
        }
    }

    public Vector3 GetStartPos(ParallaxType piece) {
        switch (piece) {
            case ParallaxType.foreGround:
                return Vector3.zero;
            case ParallaxType.BackGround:
                return nearBgStartPos;
            case ParallaxType.Skyline:
                return skylineStartPos;
            case ParallaxType.RoadPieces:
                return roadStartPos;
            default:
                return Vector3.zero;
        }
    }

    public void SetPieceToPool(ParallaxType piece, GameObject obj) {
        switch (piece) {
            case ParallaxType.foreGround:
                break;
            case ParallaxType.BackGround:
                backgroundPieces.Add(obj);
                break;
            case ParallaxType.Skyline:
                skylinePieces.Add(obj);
                break;
            case ParallaxType.RoadPieces:
                roadPieces.Add(obj);
                break;
            default:
                break;
        }      
    }

    private void PickObjectFromPool<T>(ParallaxType piece, T caller, ref List<GameObject> pools) {
            int rnd = 0;
            if (pools.Count > 1) {
                rnd = Random.Range(0, pools.Count - 1);
            }
            GameObject parObj = pools[rnd];

            if (parObj != null) {
                if (caller is ParallaxPiece) {
                    parObj.transform.position = (caller as ParallaxPiece).ReturnFollowerPosition();
                    parObj.GetComponent<ParallaxPiece>().activated = true;
                }
            }
            pools.RemoveAt(rnd); 
        
    }

    private void LoadPrefabs() {
        GameObject[] objs;
        foreach(ParallaxType i in System.Enum.GetValues(typeof(ParallaxType))) {
            objs = Resources.LoadAll<GameObject>("Parallax/" + i.ToString());
            if (objs != null && objs.Length > 0) {
                parallaxObjs.Add(i, objs);
            }
           // pools.Add(i, new List<GameObject>());
        }
    }
}
