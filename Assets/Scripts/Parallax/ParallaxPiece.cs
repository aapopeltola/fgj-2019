﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParallaxPiece : MonoBehaviour
{
    public ParallaxType pieceType;
    public Transform sizeReference;

    private Vector3 moveDir;
    private float pieceSpeed;

    private bool followerCreated = false;
    public bool activated = false;

    private Vector3 point;

    private float midThresh {
        get {
            return ParallaxManager.instance_ != null ?
                ParallaxManager.instance_.midSectionThresh :
                0f;
        }
    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (activated) {
            this.transform.position += new Vector3((pieceSpeed * ParallaxManager.instance_.masterSpeed) * moveDir.x, 0, 0);
            ReadyForNextPiece();
            RemoveCheck();
        }
    }

    public void ReadyUp(Vector3 startpos, Vector2 moveDir, float pieceSpeed) {
        this.pieceSpeed = pieceSpeed;
        this.transform.position = startpos;
        this.moveDir = moveDir;
        activated = true;
    }

    public Vector3 ReturnFollowerPosition() {
        return this.transform.position + 
            new Vector3(ReturnWidth() * -moveDir.x, 0, 0);
    }

    private float ReturnWidth() {
        return sizeReference.lossyScale.x;
    }

    private void ReadyForNextPiece() {
        if(this.transform.position.x < midThresh && this.transform.position.x > -midThresh) {
            if (!followerCreated) {
                followerCreated = true;
                ParallaxManager.instance_.NextPiece(pieceType, this);
            }
        }
    }

    private void RemoveCheck() {
        point = ParallaxManager.instance_.sideCam.WorldToViewportPoint(this.transform.position + (ReturnWidth() * moveDir));

        if(point.x < -5 || point.x > 5) {
            activated = false;
            this.transform.position = new Vector3(10f, 50f, 0f);
            followerCreated = false;
            ParallaxManager.instance_.SetPieceToPool(pieceType, this.gameObject);
        }
    }

}
