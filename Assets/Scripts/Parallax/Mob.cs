﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine.Unity;

public class Mob : MonoBehaviour
{
    private EnemyType mobType_;
    private MobSpawner spawner;
    private SkeletonAnimation skele;
    private SkeletonGraphic graphic;
    private Lane mobLane;

    private bool collided = false;
    // Start is called before the first frame update
    void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {       
    }

    public void SetSpawner(MobSpawner owner, EnemyType mobtype, Lane lane, GameObject end) {
        spawner = owner;
        GetComponent<MeshRenderer>().sortingOrder = lane == Lane.up ? 3 : 8;
        //skele.Skeleton.SetSkin(GetSkinType(mobtype));
        //skele.Skeleton.SetSlotsToSetupPose();
        //Spine.AnimationState anim = skele.AnimationState;
        //anim.Apply(skele.skeleton);
        //skele.Initialize(true);

        mobLane = lane;
        mobType_ = mobtype;
        this.transform.localScale = lane == Lane.up ? new Vector3(0.6f, 0.6f, 0.6f) : this.transform.localScale;
        if (mobtype != EnemyType.LOW) {
            StartCoroutine(Move(end));
        }
    }

    private IEnumerator Move(GameObject endPoint) {
        while ((this.transform.position.x > (endPoint.transform.position.x + 1f) ||
                this.transform.position.x < (endPoint.transform.position.x - 1f)) && !collided) {
                 this.transform.position += (new Vector3(spawner.speed *
                (spawner.masterSpeedAffects ? ParallaxManager.instance_.masterSpeed : 1f), 0, 0) * spawner.dir.x);
            yield return null;
        }
        Destroy(this.gameObject);
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if (other.CompareTag("Player")) {
            CharacterMechanism cm;
            if ((cm = other.GetComponent<CharacterMechanism>()).GetCurrentLane() == mobLane) {
                if (mobType_ != EnemyType.LOW) {
                    //got player --> do stuff
                    ParallaxManager.instance_.EnemyCathedMe(mobType_);

                    collided = true;
                }
                else {
                    //slowdown
                    spawner.SlowDownCrowd(cm, 1f, 0.7f);
                } 
            }

        }
    }

}
