﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PropSpawner : Spawner
{
    // Start is called before the first frame update
    protected virtual void Start()
    {
        
    }

    // Update is called once per frame
    protected override void Update()
    {
        if (canSpawn_) {
            canSpawn_ = false;
            StartCoroutine(SpawnOrNot(spawnlist[Random.Range(0, spawnlist.Length)]));
        }
    }
}
