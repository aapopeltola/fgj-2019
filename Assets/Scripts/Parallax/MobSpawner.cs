﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MobSpawner : Spawner
{
    public Lane lane;
    // Start is called before the first frame update
    protected void Start()
    {
        
    }



    // Update is called once per frame
    protected override void Update()
    {
        if (canSpawn_) {
            canSpawn_ = false;
            StartCoroutine(SpawnOrNotMob(spawnlist[0], EnemyType.LOW));
        }
    }

    public void SpawnMob(EnemyType mobtype) {
        int i = 1;
        if(mobtype == EnemyType.HIGH) {
            i = 2;
        }

        SpawnMobForReal(spawnlist[i], mobtype);
    }

    protected IEnumerator SpawnOrNotMob(GameObject mob, EnemyType mobtype) {
        if (Random.Range(0f, 1f) < successPercent) {
            GameObject go = Instantiate<GameObject>(mob, this.transform);
            go.transform.position = this.transform.position;
            go.GetComponent<Mob>().SetSpawner(this, mobtype, lane, endPoint);

            while (go.transform.position.x > (endPoint.transform.position.x + 1f) ||
                go.transform.position.x < (endPoint.transform.position.x - 1f)) {
                go.transform.position += (new Vector3(speed *
                    (masterSpeedAffects ? ParallaxManager.instance_.masterSpeed : 1f), 0, 0) * dir.x);
                yield return null;
            }
            Destroy(go);
        }

        StartCoroutine(WaitTime());
    }

    private void CallBackForHit(EnemyType mobtype) {
        SpawnMob(mobtype);
    }

    private void SpawnMobForReal(GameObject mob, EnemyType mobtype) {
        GameObject go = Instantiate<GameObject>(mob);
        go.transform.position = this.transform.position;
        go.GetComponent<Mob>().SetSpawner(this, mobtype, lane, endPoint);
    }

    private IEnumerator SlowDown(CharacterMechanism cm, float duration, float speedWhenHit) {
        float time = duration;
        ParallaxManager.instance_.masterSpeed = speedWhenHit;
        cm.slowdownEffectOn = true;
        while(time > 0) {
            time -= Time.deltaTime;
            yield return null;
        }

        ParallaxManager.instance_.masterSpeed = 1f;
        cm.slowdownEffectOn = false;
    }

    public void SlowDownCrowd(CharacterMechanism cm, float duration, float speedWhenHit) {
        StartCoroutine(SlowDown(cm, duration, speedWhenHit));
    }
}
